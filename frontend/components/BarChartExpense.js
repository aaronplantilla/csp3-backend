import { useState, useEffect } from 'react'
import { Bar } from 'react-chartjs-2'
import moment from 'moment'
import AppHelper from '../app-helper'

export default function index(){
	const [months, setMonths] = useState([])
    const [monthlyExpense, setMonthlyExpense] = useState([])
    const [rawData, setRawData] = useState([])
    let tempMonths = []

	useEffect(() => {
		fetch(`${ AppHelper.API_URL }/users/get-most-recent-records`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
			}
		}).then(AppHelper.toJSON)
		.then(data => {
			setRawData(data)
			// Push months data
			data.forEach(element => {
				if(!tempMonths.find(month => month === moment(element.dateAdded).format('MMMM'))){
					tempMonths.push(moment(element.dateAdded).format('MMMM'))
				}
			})

			const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

			tempMonths.sort((a, b) => {
				if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1){
					return monthsRef.indexOf(a) - monthsRef.indexOf(b)
				}
	    	})

	    	setMonths(tempMonths)

		})
    			
    }, [])

    useEffect(() => {
		setMonthlyExpense(months.map(month => {
			let income = 0
			rawData.forEach(element => {
				if(moment(element.dateAdded).format('MMMM') === month && element.typeName === 'Expense'){
					income = income + parseInt(element.amount)
				}
			})
			return income
		}))
	}, [months])

	const data = {
		labels: months,
		datasets: [
			{
				label: 'Monthly Expense',
                backgroundColor: 'rgba(255,99,132,0.4)',
                borderColor: 'rgba(255,99,132,1)',
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                hoverBorderColor: 'rgba(255,99,132,1)',
                data: monthlyExpense
			}
		]
	}

	console.log(monthlyExpense)

    return(
    	<Bar data={data}/>
    )
}