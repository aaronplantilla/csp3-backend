import { useState, useEffect } from 'react'
import { Bar } from 'react-chartjs-2'
import moment from 'moment'
import AppHelper from '../app-helper'

export default function index(){
	const [months, setMonths] = useState([])
    const [monthlyIncome, setMonthlyIncome] = useState([])
    const [rawData, setRawData] = useState([])
    let tempMonths = []

	useEffect(() => {
		fetch(`${ AppHelper.API_URL }/users/get-most-recent-records`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
			}
		}).then(AppHelper.toJSON)
		.then(data => {
			setRawData(data)
			// Push months data
			data.forEach(element => {
				if(!tempMonths.find(month => month === moment(element.dateAdded).format('MMMM'))){
					tempMonths.push(moment(element.dateAdded).format('MMMM'))
				}
			})

			const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

			tempMonths.sort((a, b) => {
				if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1){
					return monthsRef.indexOf(a) - monthsRef.indexOf(b)
				}
	    	})

	    	setMonths(tempMonths)

		})
    			
    }, [])

    useEffect(() => {
		setMonthlyIncome(months.map(month => {
			let income = 0
			rawData.forEach(element => {
				if(moment(element.dateAdded).format('MMMM') === month && element.typeName === 'Income'){
					income = income + parseInt(element.amount)
				}
			})
			return income
		}))
	}, [months])

	const data = {
		labels: months,
		datasets: [
			{
				label: 'Monthly Income',
                backgroundColor: 'rgba(123, 239, 178, 1)',
                borderColor: 'rgba(77, 175, 124, 1)',
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(123, 239, 178, 1)',
                hoverBorderColor: 'rgba(77, 175, 124, 1)',
                data: monthlyIncome
			}
		]
	}

	console.log(monthlyIncome)

    return(
    	<Bar data={data}/>
    )
}