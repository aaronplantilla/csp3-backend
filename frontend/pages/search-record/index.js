import { useState, useEffect } from 'react';
import { Container, Form, Card } from 'react-bootstrap';
import Head from 'next/head'
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper'
import Link from 'next/link'
import moment from 'moment'

export default function index(){
	const [records, setRecords] = useState([])
	const [searchData, setSearchData] = useState('')
	const [showRecord, setShowRecord] = useState([])

	useEffect(() => {
		fetch(`${ AppHelper.API_URL }/users/get-most-recent-records`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
			}
		}).then(AppHelper.toJSON)
		.then(data => {
			setRecords(data)
	    })

	}, [records])

	useEffect(() => {
		// console.log(records)
		if(searchData !== ''){
			const param = new RegExp(searchData.toLowerCase())
			setShowRecord(records.filter(record => {
				return(
					param.test(record.categoryName.toLowerCase())
				||	param.test(record.typeName.toLowerCase())
				||	param.test(record.description.toLowerCase())
				||	param.test(moment(record.dateAdded).format('MMMM DD, YYYY'))
				||	param.test(record.amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
				||	param.test(record.balanceAfterTransaction.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
				)
				
			}))
		}else{
			setSearchData('')
		}
	}, [searchData, records])

	const cardData = showRecord.map(show => {
		return(
			<React.Fragment>
			<Card.Title key={show._id}>{show.typeName}</Card.Title>
				<Card.Text key={show._id}>
					<span className="subtitle">Category Name:</span>
					<br />
					{show.categoryName}
					<br />
					<span className="subtitle">Description:</span>
					<br />
					{show.description}
					<br />
					<span className="subtitle">Date Added:</span>
					<br />
					{moment(show.dateAdded).format('LLL')}
					<br />
					<span className="subtitle">Amount:</span>
					<br />
					{show.typeName == 'Income' ?

					<span className="text-success">&#8369; {show.amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>
					:
					<span className="text-danger">&#8369; {show.amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span>

					}
					<br />
					<span className="subtitle">Balance After Transaction:</span>
					<br />
					{show.balanceAfterTransaction}
					<br />
				</Card.Text>
			</React.Fragment>
		)
	})

	return(
	<React.Fragment>
	<Form>
		<Form.Group controlId = "country">
			<Form.Control type="text" placeholder="Search for any records" value={ searchData } onChange={e => setSearchData(e.target.value)} />
		</Form.Group>
	</Form>
	<Card>
		<Card.Body>
			{cardData}
		</Card.Body>
	</Card>
	</React.Fragment>
	)
}