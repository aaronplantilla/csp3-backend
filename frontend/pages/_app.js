import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';

import '../styles/globals.css';
import "bootswatch/dist/Sketchy/bootstrap.min.css";

import { UserProvider } from '../UserContext';
import AppHelper from '../app-helper';
import NavBar from '../components/NavBar'

function MyApp({ Component, pageProps }) {

	const [user, setUser] = useState({
		email: null
	})

	useEffect(() => {
		const accessToken = localStorage.getItem('token')

		const options = {
            headers: { Authorization: `Bearer ${ accessToken }` } 
        }

        fetch('http://localhost:4000/api/users/details', options).then(res => res.json()).then(data => {
            setUser({ email: data.email })
        })
        
	}, [user.email])

	const unsetUser = () => {
		localStorage.clear();

		setUser({
			email: null
		})
	}

  return(
  	<UserProvider value={{user, setUser, unsetUser}}>
  		<NavBar/>
	  	<Container fluid id="mainCont">
	  		<Component {...pageProps} />
	  	</Container>
  	</UserProvider>
  )
}

export default MyApp
