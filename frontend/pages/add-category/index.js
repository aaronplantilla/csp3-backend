import {useState, useEffect} from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import View from '../../components/View'
import Router from 'next/router'
import Head from 'next/head'
import Swal from 'sweetalert2'
import PropTypes from 'prop-types';

export default function index() {

	const [category, setCategory] = useState('')
	const [categoryType, setCategoryType] = useState('')
 
    //state to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false)

    useEffect(() => {
        if(category !== '' && categoryType !== '' ){
            setIsActive(true);
        }else{
            setIsActive(false)
        }
    }, [category, categoryType])

	//function to add category
	function newCategory(e) {
		e.preventDefault();

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/add-category`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: category,
				typeName: categoryType
			})
		})
		.then(res => res.json())
		.then(data => {
			//add successful
			if(data === true){
				Swal.fire({
				  title: 'Nyan!!',
				  text: 'Category has been added!',
				  imageUrl: 'https://media1.tenor.com/images/68acc33e723b36bc3bfaf201cb22003f/tenor.gif?itemid=5139438',
				  imageAlt: 'Custom image',
				})
				Router.push('/categories')
			}else{
				//error in creating registration, redirect to error page
				Swal.fire({
				  icon: 'error',
				  title: 'Woops!',
				  text: 'Adding a new category failed!',
				})
			}
		})
	} 

    return (
    	<React.Fragment>
	    	<View title={ 'Add New Category | The Thrifty Cat' } />
	    	<Container>
		    	<Form onSubmit={(e) => newCategory(e)}>
		    	 	<Form.Group controlId="category">
		                <Form.Label  className="text-light">Category</Form.Label>
		                <Form.Control type="text" value={category} onChange={e => setCategory(e.target.value)} required/>
		                <Form.Text className="text-muted">
	                    	Category examples: Gaming, Food, Fitness, etc.
	                    </Form.Text>
		            </Form.Group>
		            <Form.Group controlId="type">
		            	<Form.Label  className="text-light">Type</Form.Label>
						<Form.Control as="select" value={categoryType} onChange={e => setCategoryType(e.target.value)}>
							<option value="Income">Income</option>
							<option value="Expense">Expense</option>
						</Form.Control>
		            </Form.Group>

		            {isActive ?
		                <Button variant="success" type="submit" id="submitBtn">Add</Button>
		                :
		                <Button variant="primary" type="submit" id="submitBtn" disabled>Add</Button>
		            }
		        </Form>
	    	</Container>
    	</React.Fragment>
    )
}