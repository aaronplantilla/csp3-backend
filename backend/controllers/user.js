const User = require('../models/user')
const auth = require('../auth')
const { OAuth2Client } = require('google-auth-library')
const CLIENT_ID = '127553766881-e4v2v6lc881gk4jocdq9lkllhro6lhps.apps.googleusercontent.com'
const moment = require('moment')
const bcrypt = require('bcrypt')

//function to catch & handle errors
const errCatcher = err => console.log(err)

//function for finding duplicate emails
module.exports.emailExists = (params) => {
    //find a user document with matching email
    return User.find({ email: params.email })
    .then(result => {
        //if match found, return true
        return result.length > 0 ? true : false
    })
    .catch(errCatcher)
}

module.exports.addCategory = (params) => {
    return User.findById(params.userId).then(user => {
        user.categories.push({ name: params.name, typeName: params.typeName })

        return user.save().then((user, err) => {
            return (err) ? false : true
        })
    })
} 

module.exports.addRecord = (params) => {
    return User.findById(params.userId).then(user => {
        let balanceAfterTransaction = 0

        if (user.transactions.length !== 0) {
            const balanceBeforeTransaction = user.transactions[user.transactions.length - 1].balanceAfterTransaction
            
            if (params.typeName === 'Income') {
                balanceAfterTransaction = parseInt(balanceBeforeTransaction) + parseInt(params.amount)
            }
            else {
                balanceAfterTransaction = balanceBeforeTransaction - params.amount
            }
        } else {
            balanceAfterTransaction = params.amount
        }

        user.transactions.push({
            categoryName: params.categoryName,
            typeName: params.typeName,
            amount: params.amount,
            description: params.description,
            balanceAfterTransaction: balanceAfterTransaction
        })

        return user.save().then((user, err) => {
            return (err) ? false : true
        })
    })
}

module.exports.get = (params) => {
    return User.findById(params.userId).then(user => {
        return { email: user.email }
    })
}

module.exports.getCategories = (params) => {
    return User.findById(params.userId).then(user => {
        if (typeof params.typeName  === 'undefined') {
            return user.categories
        }

        return user.categories.filter((category) => {
            if (category.typeName === params.typeName) {
                return category
            }
        })
    })

}

module.exports.getMostRecentRecords = (params) => {
    return User.findById(params.userId).then(user => {
        return  user.transactions.reverse() // .slice(0, 5) to retrieve first 5 records.
    })
}

module.exports.getRecordsByRange = (params) => {
    return User.findById(params.userId).then(user => {
        const recentRecords = user.transactions.filter((transaction) => {
            const isSameOrAfter = moment(transaction.dateAdded).isSameOrAfter(params.fromDate, 'day')
            const isSameOrBefore = moment(transaction.dateAdded).isSameOrBefore(params.toDate, 'day')

            if (isSameOrAfter && isSameOrBefore) {
                return transaction
            }
        })
        return recentRecords
    })
}

module.exports.getRecordsBreakdownByRange = (params) => {
    return User.findById(params.userId).then(user => {
        const summary = user.categories.map((category) => {
            return { categoryName: category.name, totalAmount: 0 }
        })

        user.transactions.filter((transaction) => {
            const isSameOrAfter = moment(transaction.dateAdded).isSameOrAfter(params.fromDate, 'day')
            const isSameOrBefore = moment(transaction.dateAdded).isSameOrBefore(params.toDate, 'day')

            if (isSameOrAfter && isSameOrBefore) {
                for (let i = 0; i < summary.length; i++) {
                    if (summary[i].categoryName === transaction.categoryName) {
                        summary[i].totalAmount += transaction.amount
                    }
                }
            }
        })

        return summary
    })
}

module.exports.searchRecord = (params) => {
    let records = []

    return User.findById(params.userId).then(user => {
        if (params.searchType === 'All') {
            records = user.transactions.filter((transaction) => {
                const hasIncludedKeyword = transaction.description.toLowerCase().includes(params.searchKeyword.toLowerCase())

                if (hasIncludedKeyword) {
                    return transaction
                }
            })
        } else {
            records = user.transactions.filter((transaction) => {
                const hasIncludedKeyword = transaction.description.toLowerCase().includes(params.searchKeyword.toLowerCase())
                const isSelectedType = transaction.typeName === params.searchType

                if (hasIncludedKeyword && isSelectedType) {
                    return transaction
                }
            })
        }

        return records.reverse()
    })
}

module.exports.verifyGoogleTokenId = async (tokenId) => {
    const client = new OAuth2Client(CLIENT_ID)
    const data = await client.verifyIdToken({ idToken: tokenId, audience: CLIENT_ID })

    if (data.payload.email_verified === true) {
        const user = await User.findOne({ email: data.payload.email }).exec()

        if (user !== null) {
            return { accessToken: auth.createAccessToken(user.toObject()) }
        } else {
            const newUser = new User({
                firstName: data.payload.given_name,
                lastName: data.payload.family_name,
                email: data.payload.email
            })

            return newUser.save().then((user, err) => {
                return { accessToken: auth.createAccessToken(user.toObject()) }
            })
        }
    } else {
        return { error: 'google-auth-error' }
    }
}

//registration
module.exports.register = (params) => {
    let newUser = new User({
        firstName: params.firstName,
        lastName: params.lastName,
        email: params.email,
        password: bcrypt.hashSync(params.password, 10),
    })

    return newUser.save().then((user, err) => {
        return (err) ? false: true
    })
}

//login
module.exports.login = (params) => {
    return User.findOne({email: params.email}).then(resultFromFindOne => {
        if(resultFromFindOne === null){ //user does not exist
            return { error: 'does-not-exist' }
        }
        
        const isPasswordMatched = bcrypt.compareSync(params.password, resultFromFindOne.password)
        //compareSync() is used to compare a non-hashed password to a hashed password, and it returns a boolean value (true or false)

        if(isPasswordMatched){
            return {accessToken: auth.createAccessToken(resultFromFindOne.toObject())}
            //auth is a custom module that uses JSON web token to pass data around 
            //createAccessToken is a method in auth that creates a JWT with the user's information as part of the token
        }else{
            return { error: 'incorrect-password' }
        }

    })
}